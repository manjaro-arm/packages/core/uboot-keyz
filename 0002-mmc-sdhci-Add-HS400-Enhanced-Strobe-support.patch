From: Alper Nebi Yasak <alpernebiyasak@gmail.com>
To: u-boot@lists.denx.de
Subject: [PATCH v3 1/4] mmc: sdhci: Add HS400 Enhanced Strobe support
Date: Sun, 16 Jan 2022 23:18:10 +0300

Delegate setting the Enhanced Strobe configuration to individual drivers
if they set a function for it. Return -ENOTSUPP if they do not, like
what the MMC uclass does.

Signed-off-by: Alper Nebi Yasak <alpernebiyasak@gmail.com>
Reviewed-by: Jaehoon Chung <jh80.chung@samsung.com>
---

(no changes since v2)

Changes in v2:
- Add tag: "Reviewed-by: Jaehoon Chung <jh80.chung@samsung.com>"

 drivers/mmc/sdhci.c | 18 ++++++++++++++++++
 include/sdhci.h     |  1 +
 2 files changed, 19 insertions(+)

diff --git a/drivers/mmc/sdhci.c b/drivers/mmc/sdhci.c
index 766e4a6b0c5e..bf989a594f7e 100644
--- a/drivers/mmc/sdhci.c
+++ b/drivers/mmc/sdhci.c
@@ -513,6 +513,7 @@ void sdhci_set_uhs_timing(struct sdhci_host *host)
 		reg |= SDHCI_CTRL_UHS_SDR104;
 		break;
 	case MMC_HS_400:
+	case MMC_HS_400_ES:
 		reg |= SDHCI_CTRL_HS400;
 		break;
 	default:
@@ -666,6 +667,7 @@ static int sdhci_set_ios(struct mmc *mmc)
 		    mmc->selected_mode == MMC_DDR_52 ||
 		    mmc->selected_mode == MMC_HS_200 ||
 		    mmc->selected_mode == MMC_HS_400 ||
+		    mmc->selected_mode == MMC_HS_400_ES ||
 		    mmc->selected_mode == UHS_SDR25 ||
 		    mmc->selected_mode == UHS_SDR50 ||
 		    mmc->selected_mode == UHS_SDR104 ||
@@ -799,6 +801,19 @@ static int sdhci_wait_dat0(struct udevice *dev, int state,
 	return -ETIMEDOUT;
 }
 
+#if CONFIG_IS_ENABLED(MMC_HS400_ES_SUPPORT)
+static int sdhci_set_enhanced_strobe(struct udevice *dev)
+{
+	struct mmc *mmc = mmc_get_mmc_dev(dev);
+	struct sdhci_host *host = mmc->priv;
+
+	if (host->ops && host->ops->set_enhanced_strobe)
+		return host->ops->set_enhanced_strobe(host);
+
+	return -ENOTSUPP;
+}
+#endif
+
 const struct dm_mmc_ops sdhci_ops = {
 	.send_cmd	= sdhci_send_command,
 	.set_ios	= sdhci_set_ios,
@@ -808,6 +823,9 @@ const struct dm_mmc_ops sdhci_ops = {
 	.execute_tuning	= sdhci_execute_tuning,
 #endif
 	.wait_dat0	= sdhci_wait_dat0,
+#if CONFIG_IS_ENABLED(MMC_HS400_ES_SUPPORT)
+	.set_enhanced_strobe = sdhci_set_enhanced_strobe,
+#endif
 };
 #else
 static const struct mmc_ops sdhci_ops = {
diff --git a/include/sdhci.h b/include/sdhci.h
index c718dd7206c1..7a65fdf95d30 100644
--- a/include/sdhci.h
+++ b/include/sdhci.h
@@ -272,6 +272,7 @@ struct sdhci_ops {
 	int (*platform_execute_tuning)(struct mmc *host, u8 opcode);
 	int (*set_delay)(struct sdhci_host *host);
 	int	(*deferred_probe)(struct sdhci_host *host);
+	int	(*set_enhanced_strobe)(struct sdhci_host *host);
 };
 
 #define ADMA_MAX_LEN	65532
