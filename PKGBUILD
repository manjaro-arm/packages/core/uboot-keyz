# U-Boot: Radxa Keyz Based on Rock Pi 4C PKGBUILD
# Maintainer: Furkan Kardame <f.kardame@manjaro.org>
# Contributor: Kevin Mihelich 
# Contributor: Dragan Simic <dsimic@buserror.io>

pkgname=uboot-keyz
pkgver=2022.01
pkgrel=1
_tfaver=2.6
pkgdesc="U-Boot for Radxa Keyz"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
makedepends=('git' 'arm-none-eabi-gcc' 'dtc' 'bc')
provides=('uboot')
conflicts=('uboot')
install=${pkgname}.install
source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-${_tfaver}.tar.gz"
        "0001-phy-Track-power-on-and-init-counts-in-uclass.patch"
        "0002-mmc-sdhci-Add-HS400-Enhanced-Strobe-support.patch"
        "0003-rockchip-sdhci-Fix-RK3399-eMMC-PHY-power-cycling.patch"
        "0004-rockchip-sdhci-Add-HS400-Enhanced-Strobe-support-for-RK3399.patch"
        "0005-rockchip-sdhci-Add-HS400-Enhanced-Strobe-support-for-RK3568.patch"
	"0006-add-initial-support-radxa-keyz.patch")
sha256sums=('81b4543227db228c03f8a1bf5ddbc813b0bb8f6555ce46064ef721a6fc680413'
            '4e59f02ccb042d5d18c89c849701b96e6cf4b788709564405354b5d313d173f7'
            'feb6ed9924f1b8f76066002cf3e414592e9ac8977f8e7aca8adc0c3d8bdb687f'
            '68ff1130b396cf106ee16fd2e697fc61eb6fb3e873d78551b8247de73386ed1e'
            'ee52b4374f159e648b6b21e40b399804c763bb3ddde60b016fcd6e0da8135f2f'
            '17e4adb43ac13a1297b5df8e1b7d3d03b555f5b5bc2085647e105cbd0897a238'
            'dd87bb8f7f5f7bdfdb3294b371c9aae2ab395c395ba1875f5772d0533c60b07f'
            'f04fdaedb11e6717533b3da85db857e30557436a765b9de3f4d95c14b42400a7')

prepare() {
  cd u-boot-${pkgver/rc/-rc}
  patch -N -p1 -i "${srcdir}/0001-phy-Track-power-on-and-init-counts-in-uclass.patch"                # Fix USB issues
  patch -N -p1 -i "${srcdir}/0002-mmc-sdhci-Add-HS400-Enhanced-Strobe-support.patch"                 # Fix eMMC detection (patchset)
  patch -N -p1 -i "${srcdir}/0003-rockchip-sdhci-Fix-RK3399-eMMC-PHY-power-cycling.patch"
  patch -N -p1 -i "${srcdir}/0004-rockchip-sdhci-Add-HS400-Enhanced-Strobe-support-for-RK3399.patch"
  patch -N -p1 -i "${srcdir}/0005-rockchip-sdhci-Add-HS400-Enhanced-Strobe-support-for-RK3568.patch"
  patch -N -p1 -i "${srcdir}/0006-add-initial-support-radxa-keyz.patch"
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  cd trusted-firmware-a-${_tfaver}

  echo -e "\nBuilding TF-A for Radxa Keyz...\n"
  make PLAT=rk3399
  cp build/rk3399/release/bl31/bl31.elf ../u-boot-${pkgver/rc/-rc}

  cd ../u-boot-${pkgver/rc/-rc}

  echo -e "\nBuilding U-Boot for Radxa Keyz...\n"
  make radxa-keyz-rk3399_defconfig

  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
  update_config 'CONFIG_MMC_SPEED_MODE_SET' 'y'
  update_config 'CONFIG_MMC_IO_VOLTAGE' 'y'
  update_config 'CONFIG_MMC_UHS_SUPPORT' 'y'
  update_config 'CONFIG_MMC_HS400_ES_SUPPORT' 'y'
  update_config 'CONFIG_MMC_HS400_SUPPORT' 'y'
  update_config 'CONFIG_MMC_SDHCI_SDMA' 'y'

  make EXTRAVERSION=-${pkgrel}
}

package() {
  cd u-boot-${pkgver/rc/-rc}

  mkdir -p "${pkgdir}/boot/extlinux"
  install -D -m 0644 idbloader.img u-boot.itb -t "${pkgdir}/boot"
}
